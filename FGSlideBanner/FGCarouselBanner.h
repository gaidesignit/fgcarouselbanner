//
//  FGCarouselBanner.h
//  FGSlideBanner
//
//  Created by fabio on 17/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@protocol FGCarouselDelegate <NSObject>
@required
- (void)FGCarouselDidScrollAtIndex:(int)index;
- (void)FGCarouselTapAtIndex:(int)index;
@end

@interface FGCarouselBanner : UIScrollView<UIScrollViewDelegate>{
    
    //objects
    UIScrollView *scrollView;
    UIPageControl* pageControl;
    NSTimer *Timer;
    
    //abstract
    CGFloat regsteredTime;
    UIColor *baseColor;
    
    //delegate
    id <FGCarouselDelegate> FGCarouselDelegate;
    
    
}
@property (retain) id FGCarouselDelegate;

@property  NSMutableArray *images;
-(id)initWithArray:(NSMutableArray *)array andFrame:(CGRect)frame andTintColor:(UIColor *)color;
-(void)setAutoRepeater:(BOOL)boolean withTimer:(CGFloat)time;
-(void)CarouselInvalidate;
@end
