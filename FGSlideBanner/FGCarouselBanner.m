//
//  FGCarouselBanner.m
//  FGSlideBanner
//
//  Created by fabio on 17/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import "FGCarouselBanner.h"
#define SLIDER_WIDTH self.frame.size.width
#define SLIDER_HEIGHT self.frame.size.height

@implementation FGCarouselBanner
@synthesize FGCarouselDelegate;

-(id)initWithArray:(NSMutableArray *)array andFrame:(CGRect)frame andTintColor:(UIColor *)color
{
    self = [super init];
    if (self) {
        self.frame = frame;
        _images = array;
        baseColor = color;
         [self Initialize];
    }
    return self;
}

-(void)Initialize{
   
    self.pagingEnabled = YES;
    [self setShowsHorizontalScrollIndicator:NO];
    [self setShowsVerticalScrollIndicator:NO];
    [self setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    self.delegate=self;
    
    UITapGestureRecognizer *tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapAtIndex:)];
    [self addGestureRecognizer:tap];
    
    NSUInteger numberOfImages =[_images count];
    
    for (int i = 0; i < numberOfImages; i++) {
        CGFloat xOrigin = i * SLIDER_WIDTH;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin,0,SLIDER_WIDTH,SLIDER_HEIGHT)];
        if([_images[i] hasPrefix:@"http://"]){
            __weak typeof(imageView) weak = imageView;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [imageView setImageWithURL:[NSURL URLWithString:_images[i]] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                weak.contentMode = UIViewContentModeScaleAspectFill;
            }];
        }else{
            [imageView setImage:[UIImage imageNamed:_images[i]]];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        imageView.tag=i;
        imageView.clipsToBounds=YES;
        
        [self addSubview:imageView];
    }
    
    self.contentSize = CGSizeMake(numberOfImages * SLIDER_WIDTH, SLIDER_HEIGHT);
    
    // Init Page Control
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake(0, SLIDER_HEIGHT-20, SLIDER_WIDTH, 20);
    pageControl.numberOfPages = numberOfImages;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor=baseColor;
    pageControl.pageIndicatorTintColor= [UIColor whiteColor];
    [self addSubview:pageControl];
}
-(void)setAutoRepeater:(BOOL)boolean withTimer:(CGFloat)time{
    
    if (boolean) {
        regsteredTime = time;
        Timer =[NSTimer scheduledTimerWithTimeInterval:time
                                         target:self
                                       selector:@selector(updateCarousel:)
                                       userInfo:nil
                                        repeats:YES];

    }
}

-(void)updateCarousel:(NSTimer *)theTimer {
    
    NSInteger page = [self getCurrentPageIndex]+1;
    NSInteger numberOfViews = [_images count];
    
    if (page < numberOfViews) {
        
        [self scrollRectToVisible:CGRectMake(page * self.frame.size.width, 0, self.frame.size.width, self.frame.size.height) animated:YES];
        [[self FGCarouselDelegate] FGCarouselDidScrollAtIndex:(int)page];
        
    }else{
        
        [self scrollRectToVisible:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) animated:YES];
        [[self FGCarouselDelegate] FGCarouselDidScrollAtIndex:0];
    }
    
}

-(NSInteger)getCurrentPageIndex{
    
    CGFloat pageWidth = self.frame.size.width;
    float fractionalPage = self.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    return page;
}

#pragma mark tap actions

- (void) resetTimer
{
    [self timerInvalidate];
    if (regsteredTime)
        Timer = [NSTimer scheduledTimerWithTimeInterval: regsteredTime
                                                    target: self
                                                  selector:@selector(updateCarousel:)
                                                  userInfo: nil repeats:YES];
}
-(void)TapAtIndex:(id)sender{
    NSInteger page = [self getCurrentPageIndex];
    [[self FGCarouselDelegate] FGCarouselTapAtIndex:page];
}


#pragma mark scrollview delegate 

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    
    CGRect pageControlFrame = pageControl.frame;
    pageControlFrame.origin.x = self.contentOffset.x;
    pageControl.frame = pageControlFrame;
    
    NSInteger page = [self getCurrentPageIndex];
    pageControl.currentPage=(int)page;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSInteger page = [self getCurrentPageIndex];
    [[self FGCarouselDelegate] FGCarouselDidScrollAtIndex:(int)page];
    [self resetTimer];
}
- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self timerInvalidate];
}

#pragma mark timer
-(void)timerInvalidate{
    if(Timer) {
        [Timer invalidate];
        Timer = nil;
    }
}
#pragma mark carousel ivalidate
-(void)CarouselInvalidate{
    [self timerInvalidate];
    [self removeFromSuperview];
}

@end
