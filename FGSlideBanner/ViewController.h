//
//  ViewController.h
//  FGSlideBanner
//
//  Created by fabio on 17/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGCarouselBanner.h"

@interface ViewController : UIViewController<FGCarouselDelegate>{
    FGCarouselBanner *carousel;
    NSMutableArray *imagesArray;
}

@end

