//
//  ViewController.m
//  FGSlideBanner
//
//  Created by fabio on 17/03/15.
//  Copyright (c) 2015 fabio. All rights reserved.
//

#import "ViewController.h"
#import "SDImageCache.h"

@interface ViewController ()

@end

@implementation ViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //alloco il carousel banner e lo inizializzo con un array di immagini, un frame e un colore di base (esempio per pageControl)
    carousel = [[FGCarouselBanner alloc] initWithArray:imagesArray andFrame:CGRectMake(0, 0, self.view.frame.size.width, 270) andTintColor:[UIColor greenColor]];
    
    //setto il delegate del carousel
    carousel.FGCarouselDelegate=self;
    
    //setto lo scroll automatico e il la durata per slide
    [carousel setAutoRepeater:YES withTimer:4.0];
    
    //aggiungo il carousel alla vista
    [self.view addSubview:carousel];
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //invalido il carousel prima di lasciare il controller
    [carousel CarouselInvalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    imagesArray = [[NSMutableArray alloc] initWithArray:@[@"http://static.pexels.com/wp-content/uploads/2015/02/black-and-white-emergency-fire-escape-4763.jpg",
                                                                          @"1.jpg",
                                                                          @"http://static.pexels.com/wp-content/uploads/2014/06/beach-child-footprint-813-233x350.jpg",
                                                                          @"3.jpg",
                                                                          @"http://static.pexels.com/wp-content/uploads/2015/02/art-graffiti-man-4776-494x350.jpg"]];
    
}

#pragma  mark tap actions carousel

-(void)FGCarouselDidScrollAtIndex:(int)index{
    
    //evento ad ogni slide che segnala l'index attivo
    NSLog(@"Scroll at index: %i",index);
}

-(void)FGCarouselTapAtIndex:(int)index{
    
    //evento al tap sull'immagine attiva
    NSLog(@"Tap at index: %i",index);
}


@end
